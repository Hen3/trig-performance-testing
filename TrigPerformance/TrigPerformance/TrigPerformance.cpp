// TrigPerformance.cpp : This file contains the 'main' function. Program execution begins and ends there.


#include <iostream>
#include <chrono>
#include <math.h>
#define PI 3.1415926535897932384626433832795


// definitions for table sizes
#define TableSize 128
#define InterpolatedTableSize 16

// variables to controll Integral and error calculations
double To = 2.0 * PI;
int steps = INT32_MAX;

// chrono de-verboseifying
#define Clock std::chrono::high_resolution_clock
#define InSeconds std::chrono::duration_cast<std::chrono::seconds>

// Forward definitions for functions to be used in main
double SinQuadraticApprox(double t);
template <int n>
double SinLookup(double t);
template <int n>
double SinInterpolatedTable(double t);
double IntegralOf(double from, double to, int steps, double(*func)(double x));
double SinCubicApprox(double t);
double Error(double from, double to, int steps, double(*approx)(double x), double(*func)(double x));
double MaxError(double from, double to, int steps, double(*approx)(double x), double(*func)(double x));

int main()
{


    // Calculate and display the Area for all the approximations using Riemann summ approximations.
    double Area = 0.0;
    auto t1 = Clock::now();
    double SinArea = IntegralOf(0.0, To, steps, &std::sin);
    std::cout << "Area of std::sin = " << SinArea << std::endl;
    auto t2 = Clock::now();
    Area = IntegralOf(0.0, To, steps, &SinLookup<TableSize>);
    std::cout << "Area of SinLookup = " << Area << std::endl;
    auto t3 = Clock::now();
    Area = IntegralOf(0.0, To, steps, &SinInterpolatedTable<InterpolatedTableSize>);
    std::cout << "Area of SinInterpolatedTable = " << Area << std::endl;
    auto t4 = Clock::now();
    Area = IntegralOf(0.0, To, steps, &SinQuadraticApprox);
    std::cout << "Area of SinQuadraticApprox = " << Area << std::endl;
    auto t5 = Clock::now();
    Area = IntegralOf(0.0, To, steps, &SinCubicApprox);
    std::cout << "Area of SinCubicApprox = " << Area << std::endl;
    auto t6 = Clock::now();
    
    // Display the time std::sin took in the area calculation
    std::cout << "std::sin :max y-value error = 0.0 , area error = 0.0 %, time for integral = " << InSeconds(t2 - t1).count() << " s\n";

    // Calculate the Error in area and y-value for the SinLookup function for a Table with "TableSize" entries.
    double LookupArea = Error(0.0, To, steps, &SinLookup<TableSize>, &std::sin);
    double LookupError = MaxError(0.0, To, steps, &SinLookup<TableSize>, &std::sin);

    std::cout << "SinLookup : max y-value error = " << LookupError << " , area error = " << 100.0 * LookupArea/SinArea << " %, time for integral = " << InSeconds(t3 - t2).count() << " s\n";

    // Calculate the Error in area and y-value for the SinInterpolatedTable function for a Table with "InterpollatedTableSize" entries.
    double InterpolatedTableArea = Error(0.0, To, steps, &SinInterpolatedTable<InterpolatedTableSize>, &std::sin);
    double InterpolatedTableError = MaxError(0.0, To, steps, &SinInterpolatedTable<InterpolatedTableSize>, &std::sin);

    std::cout << "SinInterpolatedTable : max y-value error = " << InterpolatedTableError << " , area error = " << 100 * InterpolatedTableArea/SinArea << " %, time for integral = " << InSeconds(t4 - t3).count() << " s\n";

    // Calculate the Error in area and y-value for the SinQuadraticApprox function
    double QuadraticApproxArea = Error(0.0, To, steps, &SinQuadraticApprox, &std::sin);
    double QuadraticError = MaxError(0.0, To, steps, &SinQuadraticApprox, &std::sin);

    std::cout << "SinQuadraticApprox : max y-value error = " << QuadraticError << " , area error = " << 100 * QuadraticApproxArea/SinArea << " %, time for integral = " << InSeconds(t5 - t4).count() << " s\n";

    // Calculate the Error in area and y-value for the SinCubicApprox function
    double CubicApproxArea = Error(0.0, To, steps, &SinCubicApprox, &std::sin);
    double CubicApproxError = MaxError(0.0, To, steps, &SinCubicApprox, &std::sin);

    std::cout << "SinCubicApprox : max y-value error = " << CubicApproxError << " , area error = " << 100 * CubicApproxArea / SinArea << " %, time for integral = " << InSeconds(t6 - t5).count() << " s\n";

    std::cin.get();
    
}

//returns the modulus of two double values
double mod(double a, double b)
{
    int n = (int)(a / b);
    return a - (n * a);
}

//Returns the second order approximation of a sine wave
double SinQuadraticApprox(double t)
{
    if (t > 2.0 * PI) return SinQuadraticApprox(mod(t, PI * 2.0));
    else if (t > PI) return -SinQuadraticApprox(t - PI);
    else
    {
        //Only accurate betwee 0 and Pi
        return (4.0 / PI) * t * (1 - t / PI);
    }
}


//Returns the third order approximation of a sine wave
double SinCubicApprox(double t)
{
    if (t > 2.0 * PI) return SinCubicApprox(mod(t, PI * 2.0));
    else if (t > PI) return -SinCubicApprox(t - PI);
    else if (t > PI / 2) return SinCubicApprox(PI - t);
    else
    {
        //Probably accurate between -Pi/2 and Pi/2, however I only trust it between 0 and Pi/2
        return (t / PI) * (3.0 - (4.0 / (PI * PI)) * (t * t));
    }
}

// returns a "close" tabulated value for sin 
template <int n>
double SinLookup(double t)
{
    //Table of values for sin
    static double Table[n];
    static bool IsInitialised = false;
    //The x-difference between each table entry.
    const double StepSize = PI * 2.0 / n ;
    //Populate table only once
    if (!IsInitialised)
    {     
        for (int i = 0; i < n; i++)
        {
            Table[i] = std::sin(i * StepSize);
        }
        IsInitialised = true;
    }
    // normalize t to the table length
    t /= StepSize;
    // integer modulus to get the index
    return Table[(int)(t) % n];
}

// returns the linearly interpolated value between the closest table entries
template <int n>
double SinInterpolatedTable(double t)
{
    //Table of values for sin
    static double Table[n];
    static bool IsInitialised = false;
    // Will be accessing Table[i + 1] and this "+1" curcumvents the need to check for i + 1 < n
    // Alternatively "static double Table[n + 1]" could be used.
    const double StepSize = PI * 2.0 / (n - 1);
    if (!IsInitialised)
    {
        for (int i = 0; i < n; i++)
        {
            Table[i] = std::sin(i * StepSize);
        }
        IsInitialised = true;
    }
    t /= StepSize;
    // Get whole number and fraction compents of t
    double index, frac;
    frac = std::modf(t, &index);
    // if you used "static double Table[n + 1]" then "int step = (int)t % n"
    int step = (int)index % (n - 1);
    // Get first value
    double a = Table[step];
    // Get second value
    double b = Table[step + 1];
    // Linear interpolation
    return frac * (b - a) + a;
}

// returns the Area between a curve and the "y=0" line.
double IntegralOf(double from, double to, int steps, double(*func)(double x))
{
    double dx = (to - from) / steps;
    double res = 0.0;
    for (int i = 0; i < steps; i++)
    {
        res += abs(dx * func(from + i * dx));
    }
    return res;
}

// returns area between two curves.
double Error(double from, double to, int steps, double(*approx)(double x), double(*func)(double x))
{
    double dx = (to - from) / steps;
    double res = 0.0;
    for (int i = 0; i < steps; i++)
    {
        res += dx * abs(approx(from + i * dx) - func(from + i * dx));
    }
    return res;
}

// returns the maximum difference between two curves in a specified region.
// It's probably smart to use an actual numeric optimization function here but it works for this proram's purpose
double MaxError(double from, double to, int steps, double(*approx)(double x), double(*func)(double x))
{
    double dx = (to - from) / steps;
    double res = 0.0;
    for (int i = 0; i < steps; i++)
    {
        double a = abs(approx(from + i * dx) - func(from + i * dx));
        if (a > res)res = a;
    }
    return res;
}

