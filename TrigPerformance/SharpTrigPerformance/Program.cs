﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace SharpTrigPerformance
{
    class Program
    {
        const int TableSize = 128;
        private static double[] SineTable = new double[TableSize + 1];
        const double StepSize = Math.PI * 2.0 / TableSize;

        private static void InitializeTable()
        {
            for(int i = 0; i < TableSize + 1; i++)
            {
                SineTable[i] = Math.Sin(i * StepSize);
            }
        }

        delegate double SineFunction(double t);

        static void Main(string[] args)
        {
            int Steps = int.MaxValue;
            //Stopwatch S = new Stopwatch();
            //S.Start();
            //InitializeTable();
            //S.Stop();
            //var InitializingTime = S.Elapsed;
            //S.Restart();
            //double SinArea = IntegralArea(0, Math.PI * 2.0, Steps, Math.Sin);
            //S.Stop();
            //var SinTime = S.Elapsed;
            //Console.WriteLine($"Math.Sin : Elapsed Time = {SinTime.TotalMilliseconds} ms, Error = 0.0%");
            //S.Restart();
            //double TableArea = IntegralArea(0, Math.PI * 2.0, Steps, SinLookup);
            //S.Stop();
            //var LookupTime = S.Elapsed;
            //Console.WriteLine($"SinLookup : Elapsed Time = {(LookupTime + InitializingTime).TotalMilliseconds} ms, Error = {100.0 * Math.Abs(TableArea - SinArea) / SinArea}%");
            //S.Restart();
            //double InterpolatedArea = IntegralArea(0, Math.PI * 2.0, Steps, SinInterpolatedLookup);
            //S.Stop();
            //var InterpolatedTime = S.Elapsed;
            //Console.WriteLine($"SinInterpolatedLookup : Elapsed Time = {(InterpolatedTime + InitializingTime).TotalMilliseconds} ms, Error = {100.0 * Math.Abs(InterpolatedArea - SinArea) / SinArea}%");
            //S.Restart();
            //double QuadraticArea = IntegralArea(0, Math.PI * 2.0, Steps, SinQuadraticApprox);
            //S.Stop();
            //var QuadraticTime = S.Elapsed;
            //Console.WriteLine($"SinQuadraticApprox : Elapsed Time = {QuadraticTime.TotalMilliseconds} ms, Error = {100.0 * Math.Abs(QuadraticArea - SinArea) / SinArea}%");
            //S.Restart();
            //double CubicArea = IntegralArea(0, Math.PI * 2.0, Steps, SinCubicApprox);
            //S.Stop();
            //var CubicTime = S.Elapsed;
            //Console.WriteLine($"SinCubicApprox : Elapsed Time = {CubicTime.TotalMilliseconds} ms, Error = {100.0 * Math.Abs(CubicArea - SinArea) / SinArea}%");

            InitializeTable();
            double SinArea = IntegralArea(0, Math.PI * 2.0, Steps, Math.Sin);
            double LookupError = MaxError(0, Math.PI * 2.0, Steps, SinLookup, Math.Sin);
            Console.WriteLine($"SinLookup Error = {100.0 * LookupError}");
            double InterpollatedError = MaxError(0, Math.PI * 2.0, Steps, SinInterpolatedLookup, Math.Sin);
            Console.WriteLine($"SinInterpolatedLookup Error = {100.0 * InterpollatedError}");
            double QuadraticError = MaxError(0, Math.PI * 2.0, Steps, SinQuadraticApprox, Math.Sin);
            Console.WriteLine($"SinInterpolatedLookup Error = {100.0 * QuadraticError}");
            Console.ReadKey();
        }

        private static double SinLookup(double t)
        {
            t *= 1 / StepSize;
            return SineTable[(int)t % TableSize];
        }

        private static double SinInterpolatedLookup(double t)
        {
            t *= 1 / StepSize;
            int _t = (int)t;
            int index = _t % TableSize;
            double a = SineTable[index];
            double b = SineTable[index + 1];
            t = t - _t;
            return a + t * (b - a);
        }

        private static double SinQuadraticApprox(double t)
        {
            if (t > Math.PI * 2.0) return SinQuadraticApprox(t % Math.PI * 2.0);
            else if (t > Math.PI) return -SinQuadraticApprox(t - Math.PI);
            else return (4.0 / Math.PI) * t * (1 - t / Math.PI);
        }

        private static double SinCubicApprox(double t)
        {
            if (t > 2.0 * Math.PI) return SinCubicApprox(t % Math.PI * 2.0);
            else if (t > Math.PI) return -SinCubicApprox(t - Math.PI);
            else if (t > Math.PI / 2) return SinCubicApprox(Math.PI - t);
            else
            {
                return (t / Math.PI) * (3.0 - (4.0 / (Math.PI * Math.PI)) * (t * t));
            }
        }

        private static double IntegralArea(double start, double stop, int steps, SineFunction sin)
        {
            double dx = (stop - start) / steps;
            double res = 0.0;
            for(int i = 0; i < steps; i ++)
            {
                res += Math.Abs(dx * sin(start + i * dx));
            }
            return res;
        }

        private static double Error(double start, double stop, int steps, SineFunction approx, SineFunction sin)
        {
            double dx = (stop - start) / steps;
            double res = 0.0;
            for (int i = 0; i < steps; i++)
            {
                res += Math.Abs(dx * (approx(start + i * dx) - sin(start + i * dx)));
            }
            return res;
        }

        private static double MaxError(double start, double stop, int steps, SineFunction approx, SineFunction sin)
        {
            double dx = (stop - start) / steps;
            double res = 0.0;
            for (int i = 0; i < steps; i++)
            {
                
                double a = Math.Abs(approx(start + i * dx) - sin(start + i * dx));
                if (a > res) res = a;
            }
            return res;
        }
    }
}
